//
//  ProvideNumberViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 05.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit


class ProvideNumberViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var phoneNumberTextFieldView: UIView!
    @IBOutlet weak var getRegistrationCodeButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField! {
        didSet {
            if self.phoneNumberTextField.text == "" {
                
                self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberInactive"))
            } else {
                
                self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberActive"))
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUi()
    }
    
    func setupUi()  {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backIcon")
         self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.phoneNumberTextField.delegate = self
        self.addGesture()
        self.phoneNumberTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.getRegistrationCodeButton.layer.cornerRadius = 10
        self.getRegistrationCodeButton.clipsToBounds = true
        self.getRegistrationCodeButton.applyGradient(colors: [UIColor(red: 145/255, green: 111/255, blue: 239/255, alpha: 1).cgColor, UIColor(red: 94/255, green: 173/255, blue: 255/255, alpha: 1).cgColor])
        let clearPhoneButton = phoneNumberTextField.setClearFieldButton(icon:  UIImage(imageLiteralResourceName: "clrearIcon"))
        clearPhoneButton.addTarget(self, action: #selector(clearPhone), for: .touchUpInside)
    }
    
    @objc func clearPhone(sender: UIButton!) {
        phoneNumberTextField.text = "+380"
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if self.phoneNumberTextField.text == "" {
            self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberInactive"))
        } else {
            self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberActive"))
           
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberActive"))
        self.phoneNumberTextField.text = "+380"
        phoneNumberTextFieldView.layer.borderWidth = 1
        phoneNumberTextFieldView.layer.borderColor = UIColor(red: 96/255, green: 147/255, blue: 253/255, alpha: 1).cgColor
    }
    
    @IBAction func sendVerificationCodeButtonPressed(_ sender: Any) {
        self.showSpinner(onView: self.view)
        let params = ["phone" : phoneNumberTextField.text]
        
        DataManager.shared.userController.requestAuthorisationCode(params: params) { (Error) in
            self.removeSpinner()
            guard let eror = Error else {
                let alert = UIAlertController(title: "Error", message: "Internet connection error" , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
            if Error?.result == "ok" {
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Registration", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "PhoneNumberVerificationViewController") as! PhoneNumberVerificationViewController
                vc.modalPresentationStyle = .fullScreen
                vc.providedPhone = self.phoneNumberTextField.text
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else {
                
                let alert = UIAlertController(title: "Error", message: Error?.errors?[0].message , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
            
            
            
        }
        
        
        
        
    }
    
    @IBAction func openLoginView(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "loginNavigationController") as! UIViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
