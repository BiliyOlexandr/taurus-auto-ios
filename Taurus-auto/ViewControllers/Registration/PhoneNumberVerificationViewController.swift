//
//  PhoneNumberVerificationViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 05.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit
import PinCodeInputView

class PhoneNumberVerificationViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var helpTextLabel: UILabel!
    @IBOutlet weak var passcodeView: UIView!
    
    var providedPhone:String?
    let pinCodeInputView: PinCodeInputView<ItemView> = .init(
           digit: 6,
           itemSpacing: 4,
           itemFactory: {
           return ItemView()
       })
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUi()
        self.addGesture()
    }
    
    func setupUi() {
        self.navigationController?.navigationBar.isTranslucent = true
        helpTextLabel.text = "Введите код подтверждения, который мы отправили вам на номер \(providedPhone ?? "")"
        
        pinCodeInputView.center = CGPoint(x: passcodeView.frame.width / 2, y: passcodeView.frame.height / 2)
        
        passcodeView.addSubview(pinCodeInputView)
        // set appearance
        pinCodeInputView.set(
            appearance: .init(
                itemSize: .init(width: 32, height: 44),
                font: .systemFont(ofSize: 24, weight: .regular),
                textColor: .black,
                backgroundColor: UIColor(red: 242/255, green: 245/255, blue: 247/255, alpha: 1),
                cursorColor: .clear,
                cornerRadius: 8
            )
        )
        pinCodeInputView.becomeFirstResponder()
        pinCodeInputView.set(changeTextHandler: { text in
            if self.pinCodeInputView.isFilled {
                var params = ["phone" : self.providedPhone,
                              "code"  : text]
                self.next(params: params as! [String : String])
            }
        })
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        pinCodeInputView.becomeFirstResponder()
    }
    

    
    func next(params: [String : String]) {
        
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.registerOrAuthoriseUser(params: params) { (User) in
            self.removeSpinner()
            if User?.result == "error"{
                let alert = UIAlertController(title: "Error", message: User?.errors?[0].message, preferredStyle: .alert)
                           alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                               switch action.style{
                               case .default:
                                   print("default")
                                   
                               case .cancel:
                                   print("cancel")
                                   
                               case .destructive:
                                   print("destructive")
                               }}))
                           
                           self.present(alert, animated: true, completion: nil)
                           return
            }
            let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            vc.modalPresentationStyle = .fullScreen
            HHTabBarView.shared.referenceUITabBarController.modalPresentationStyle = .fullScreen
            //vc.loggedUser = User
            self.present(HHTabBarView.shared.referenceUITabBarController, animated: true, completion: nil)
    }
    }
    
    @IBAction func popView(_ sender: Any) {
        self.showSpinner(onView: self.view)
        let params = ["phone" : providedPhone]
        
        DataManager.shared.userController.requestAuthorisationCode(params: params) { (Error) in
            self.removeSpinner()
            
        }
    }
}

