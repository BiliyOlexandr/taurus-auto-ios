//
//  ForgotPasswordViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 07.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var phoneNumberTextFieldView: UIView!
    @IBOutlet weak var phoneNumberTextField: UITextField! {
        didSet{
            self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberInactive"))
        }
    }
    @IBOutlet weak var sendCodeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addGesture()
        self.setupUi()
    }
    
    func setupUi() {
         self.sendCodeButton.applyGradient(colors: [UIColor(red: 145/255, green: 111/255, blue: 239/255, alpha: 1).cgColor, UIColor(red: 94/255, green: 173/255, blue: 255/255, alpha: 1).cgColor])
        self.sendCodeButton.layer.cornerRadius = 10
        self.sendCodeButton.clipsToBounds = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backIcon")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.phoneNumberTextField.delegate = self
        self.phoneNumberTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
    let clearPhoneButton = phoneNumberTextField.setClearFieldButton(icon:  UIImage(imageLiteralResourceName: "clrearIcon"))
        clearPhoneButton.addTarget(self, action: #selector(clearPhone), for: .touchUpInside)
    }
    
    @objc func clearPhone(sender: UIButton!) {
        phoneNumberTextField.text = "+380"
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberActive"))
        self.phoneNumberTextField.text = "+380"
        
        phoneNumberTextFieldView.layer.borderWidth = 1
        phoneNumberTextFieldView.layer.borderColor = UIColor(red: 96/255, green: 147/255, blue: 253/255, alpha: 1).cgColor

    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if self.phoneNumberTextField.text == "" {
            self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberInactive"))
        } else {
            self.phoneNumberTextField.setIcon(UIImage(imageLiteralResourceName: "phoneNumberActive"))
           
        }
    }
    
    @IBAction func sendNewPasswordButtonPressed(_ sender: Any) {
        let params = ["phone" : self.phoneNumberTextField.text]
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.getResetPasswordCode(params: params) { (Dictionary) in
            self.removeSpinner()
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProvideVerificationCodeViewController") as! ProvideVerificationCodeViewController
            vc.modalPresentationStyle = .fullScreen
            vc.providedPhone = self.phoneNumberTextField.text
            self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
