//
//  ProvideVerificationCodeViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 08.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit
import PinCodeInputView

class ProvideVerificationCodeViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var helpTextLabel: UILabel!
    @IBOutlet weak var pincodeView: UIView!
    
    
    var providedCode:String?
    var providedPhone:String?
    
    let pinCodeInputView: PinCodeInputView<ItemView> = .init(
        digit: 6,
        itemSpacing: 4,
        itemFactory: {
            return ItemView()
    })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUi()
        self.addGesture()
    }
    
    func setupUi() {
        self.navigationController?.navigationBar.isTranslucent = true
        helpTextLabel.text = "Введите код подтверждения, который мы отправили вам на номер \(providedPhone ?? "")"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backIcon")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        pinCodeInputView.center = CGPoint(x: pincodeView.frame.width / 2, y: pincodeView.frame.height / 2)
        
        pincodeView.addSubview(pinCodeInputView)
        // set appearance
        pinCodeInputView.set(
            appearance: .init(
                itemSize: .init(width: 32, height: 44),
                font: .systemFont(ofSize: 24, weight: .regular),
                textColor: .black,
                backgroundColor: UIColor(red: 242/255, green: 245/255, blue: 247/255, alpha: 1),
                cursorColor: .clear,
                cornerRadius: 8
            )
        )
        pinCodeInputView.set(changeTextHandler: { text in
            if self.pinCodeInputView.isFilled {
                self.providedCode = text
                self.next()
            }
        })
        
        
    }
    
    @IBAction func repeateSendCodeAction(_ sender: Any) {
        self.showSpinner(onView: self.view)
        let params = ["phone" : providedPhone]
        DataManager.shared.userController.getResetPasswordCode(params: params) { (Dictionary) in
            self.removeSpinner()
            
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        pinCodeInputView.becomeFirstResponder()
    }
    
    
    func next(){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProvideNewPasswordViewController") as! ProvideNewPasswordViewController
        vc.modalPresentationStyle = .fullScreen
        vc.providedPhone = providedPhone
        vc.providedCode = providedCode
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
