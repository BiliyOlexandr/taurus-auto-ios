//
//  LoginViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 05.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextFIeld: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var emailTextFieldView: UIView!
    @IBOutlet weak var passwordTextFieldView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addGesture()
        self.setupUi()
        
    }
    
    func setupUi(){
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backIcon")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.emailTextField.setIcon(UIImage(imageLiteralResourceName: "usernameIconInactive"))
        self.passwordTextFIeld.setIcon(UIImage(imageLiteralResourceName: "passwardIconInactive"))
        self.passwordTextFIeld.addTarget(self, action: #selector(self.passwordTextFieldDidChange), for: .editingChanged)
        self.emailTextField.addTarget(self, action: #selector(self.emailTextFieldDidChange), for: .editingChanged)
        self.logInButton.applyGradient(colors: [UIColor(red: 145/255, green: 111/255, blue: 239/255, alpha: 1).cgColor, UIColor(red: 94/255, green: 173/255, blue: 255/255, alpha: 1).cgColor])
        self.logInButton.layer.cornerRadius = 10
        self.logInButton.clipsToBounds = true
        self.emailTextField.delegate = self
        self.passwordTextFIeld.delegate = self
        self.emailTextField.tag = 1
        self.passwordTextFIeld.tag = 2
        let clearPhoneButton = passwordTextFIeld.setClearFieldButton(icon:  UIImage(imageLiteralResourceName: "clrearIcon"))
        clearPhoneButton.addTarget(self, action: #selector(clearPhone), for: .touchUpInside)
        self.loginView.backgroundColor = UIColor(patternImage: UIImage(named: "popUpImage")!)
        self.passwordView.backgroundColor = UIColor(patternImage: UIImage(named: "popUpImage")!)
    }
    
    @objc func clearPhone(sender: UIButton!) {
        passwordTextFIeld.text = ""
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        view.endEditing(true)
        passwordView.isHidden = true
    }
    
    @objc func passwordTextFieldDidChange(_ textField: UITextField) {
        self.passwordTextFIeld.textColor = .black
        if self.passwordTextFIeld.text == "" {
            self.passwordTextFIeld.setIcon(UIImage(imageLiteralResourceName: "passwardIconInactive"))
        } else {
            self.passwordTextFIeld.setIcon(UIImage(imageLiteralResourceName: "passwardIconActive"))
        }
    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func emailTextFieldDidChange(_ textField: UITextField) {
        if self.emailTextField.text == "" {
            self.emailTextField.setIcon(UIImage(imageLiteralResourceName: "usernameIconInactive"))
        } else {
            self.emailTextField.setIcon(UIImage(imageLiteralResourceName: "usernameIconActive"))
        }
    }
    
    @IBAction func registrationButtonAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Registration", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProvideNumberViewController") as! ProvideNumberViewController
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            emailTextFieldView.layer.borderWidth = 0
            emailTextFieldView.layer.borderColor = nil
        } else {
            passwordTextFIeld.layer.borderWidth = 0
            passwordTextFIeld.layer.borderColor = nil
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.passwordTextFIeld.textColor = .black
        
        if textField.tag == 1 {
            self.loginView.isHidden = true
            emailTextFieldView.layer.borderWidth = 1
            emailTextFieldView.layer.borderColor = UIColor(red: 96/255, green: 147/255, blue: 253/255, alpha: 1).cgColor
            
        }else {
            self.passwordView.isHidden = true
            passwordTextFieldView.layer.borderWidth = 1
            passwordTextFieldView.layer.borderColor = UIColor(red: 96/255, green: 147/255, blue: 253/255, alpha: 1).cgColor
        }
    }
    
    @IBAction func authoriseButtonPressed(_ sender: Any) {
        if emailTextField.text == "" {
            self.loginView.isHidden = false
            return
        }
        
        if passwordTextFIeld.text == "" {
            self.passwordView.isHidden = false
            
            
            return
        }
        
        
        let params = ["login" : self.emailTextField.text,
                      "password" : self.passwordTextFIeld.text]
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.authoriseUser(params: params) { (User) in
            self.removeSpinner()
            if User == nil{
                let alert = UIAlertController(title: "Error", message: "Internet connection error" , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }else if User?.errors == nil {
                self.passwordView.isHidden = true
                HHTabBarView.shared.referenceUITabBarController.modalPresentationStyle = .fullScreen
                HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                
                self.present(HHTabBarView.shared.referenceUITabBarController, animated: true, completion: nil)
                
            } else {
                self.passwordView.isHidden = false
                self.passwordTextFIeld.textColor = UIColor(red: 255/255, green: 44/255, blue: 44/255, alpha: 1)
            }
            
        }
    }
}
