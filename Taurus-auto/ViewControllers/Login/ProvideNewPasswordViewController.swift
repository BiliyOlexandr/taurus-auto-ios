//
//  ProvideNewPasswordViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 07.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class ProvideNewPasswordViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var saveButtom: UIButton!
    @IBOutlet weak var newPasswordTextFieldView: UIView!
    
    var providedPhone:String!
    var providedCode:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addGesture()
        self.setupUi()
    }
    
    func setupUi() {
        self.saveButtom.applyGradient(colors: [UIColor(red: 145/255, green: 111/255, blue: 239/255, alpha: 1).cgColor, UIColor(red: 94/255, green: 173/255, blue: 255/255, alpha: 1).cgColor])
        self.saveButtom.layer.cornerRadius = 10
        self.saveButtom.clipsToBounds = true
        let passVisibilityButton = self.newPasswordTextField.setRightViewButton(icon:  UIImage(imageLiteralResourceName: "watchpassIcon"))
        passVisibilityButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        newPasswordTextField.delegate = self
        
        
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        newPasswordTextFieldView.layer.borderWidth = 1
        newPasswordTextFieldView.layer.borderColor = UIColor(red: 96/255, green: 147/255, blue: 253/255, alpha: 1).cgColor
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        if self.newPasswordTextField.isSecureTextEntry {
            sender.setImage(UIImage(imageLiteralResourceName: "hidepassIcon"), for: .normal)
            self.newPasswordTextField.isSecureTextEntry = false
        }else {
            sender.setImage(UIImage(imageLiteralResourceName: "watchpassIcon"), for: .normal)
            self.newPasswordTextField.isSecureTextEntry = true
        }
    }
    @IBAction func saveNewPassword(_ sender: Any) {
        var params = ["phone" : self.providedPhone,
                      "code" : self.providedCode,
                      "password" : self.newPasswordTextField.text]
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.changeUserPassword(params: params) { (User) in
            self.removeSpinner()
            
            if User?.result != "ok"{
                let alert = UIAlertController(title: "Error", message: User?.errors?[0].message, preferredStyle: .alert)
                           alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                               switch action.style{
                               case .default:
                                   print("default")
                                   
                               case .cancel:
                                   print("cancel")
                                   
                               case .destructive:
                                   print("destructive")
                               }}))
                           
                           self.present(alert, animated: true, completion: nil)
                return
            }
            HHTabBarView.shared.referenceUITabBarController.modalPresentationStyle = .fullScreen
            self.present(HHTabBarView.shared.referenceUITabBarController, animated: true, completion: nil)
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
