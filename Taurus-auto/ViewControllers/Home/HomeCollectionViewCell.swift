//
//  HomeCollectionViewCell.swift
//  Taurus-auto
//
//  Created by Taras Gural on 15.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

extension CALayer {
  func applySketchShadow(
    color: UIColor = .black,
    alpha: Float = 0.5,
    x: CGFloat = 0,
    y: CGFloat = 2,
    blur: CGFloat = 4,
    spread: CGFloat = 0)
  {
    shadowColor = color.cgColor
    shadowOpacity = alpha
    shadowOffset = CGSize(width: x, height: y)
    shadowRadius = blur / 2.0
    if spread == 0 {
      shadowPath = nil
    } else {
      let dx = -spread
      let rect = bounds.insetBy(dx: dx, dy: dx)
      shadowPath = UIBezierPath(rect: rect).cgPath
    }
  }
}

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    var gradient = CAGradientLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initCell(image: UIImage, label: String, gradientStartColor: UIColor, gradientEndColor: UIColor, shadowColor: UIColor){
        widthConstraint.constant = image.size.width
        imageView.image = image
        cellLabel.text = label
        
        
        gradient.frame = cellBackgroundView.bounds
        gradient.startPoint = .init(x: 0, y: 0) // top left
        gradient.endPoint = .init(x: 1, y: 1)
        gradient.colors = [gradientStartColor.cgColor, gradientEndColor.cgColor]
        
        cellBackgroundView.layer.insertSublayer(gradient, at: 0)
        
        self.contentView.layer.cornerRadius = 20
        self.contentView.layer.masksToBounds = true

        self.layer.applySketchShadow(color: shadowColor, alpha: 0.4, x: 0, y: 10, blur: 20, spread: 0)
        self.layer.masksToBounds = false
    }

}



