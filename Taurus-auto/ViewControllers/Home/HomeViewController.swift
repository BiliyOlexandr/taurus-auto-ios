//
//  HomeViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 14.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var loggedInUser: UserResponse?
    var mainMenu: MainMenuItems?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var token: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DataManager.shared.userController.getMainMenu { (mainMenu) in
            self.mainMenu = mainMenu
            self.collectionView.reloadData()
            if mainMenu == nil{
                
                let alert = UIAlertController(title: "Error", message: "Internet connection error" , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func setupUi()  {
        collectionView.register(UINib.init(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.getLoggedUserInfo { (user) in
            self.removeSpinner()
            self.loggedInUser = user
            
            
        }
        
        
    }
    
}

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Clients", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ClientsViewController") as! ClientsViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainMenu?.data?.items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if loggedInUser?.data?.user?.groups?[0].name != "manager"{
            switch indexPath.item {
            case 0:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "chatsIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 31/255, green: 230/255, blue: 182/255, alpha: 1), gradientEndColor: UIColor(red: 74/255, green: 159/255, blue: 234/255, alpha: 1), shadowColor: UIColor(red: 60/255, green: 182/255, blue: 217/255, alpha: 1))
                
                return homeCollectionViewCell
            case 1:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "chooseIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 37/255, green: 148/255, blue: 252/255, alpha: 1), gradientEndColor: UIColor(red: 114/255, green: 102/255, blue: 243/255, alpha: 1), shadowColor: UIColor(red: 89/255, green: 116/255, blue: 246/255, alpha: 1))
                return homeCollectionViewCell
            case 2:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "cellServiceIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 163/255, green: 138/255, blue: 228/255, alpha: 1), gradientEndColor: UIColor(red: 246/255, green: 86/255, blue: 94/255, alpha: 1), shadowColor: UIColor(red: 198/255, green: 116/255, blue: 171/255, alpha: 1))
                return homeCollectionViewCell
                
            case 3:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "faqIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 0/255, green: 210/255, blue: 58/255, alpha: 1), gradientEndColor: UIColor(red: 20/255, green: 203/255, blue: 206/255, alpha: 1), shadowColor: UIColor(red: 10/255, green: 206/255, blue: 136/255, alpha: 1))
                return homeCollectionViewCell
            default:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "chatsIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 0/255, green: 210/255, blue: 58/255, alpha: 1), gradientEndColor: UIColor(red: 20/255, green: 203/255, blue: 206/255, alpha: 1), shadowColor: UIColor(red: 60/255, green: 182/255, blue: 217/255, alpha: 1))
                return homeCollectionViewCell
            }
        } else {
            switch indexPath.item {
            case 0:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "chooseIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 31/255, green: 230/255, blue: 182/255, alpha: 1), gradientEndColor: UIColor(red: 74/255, green: 159/255, blue: 234/255, alpha: 1), shadowColor: UIColor(red: 60/255, green: 182/255, blue: 217/255, alpha: 1))
                
                return homeCollectionViewCell
            case 1:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "chatsIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 37/255, green: 148/255, blue: 252/255, alpha: 1), gradientEndColor: UIColor(red: 114/255, green: 102/255, blue: 243/255, alpha: 1), shadowColor: UIColor(red: 89/255, green: 116/255, blue: 246/255, alpha: 1))
                return homeCollectionViewCell
            case 2:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "textIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 163/255, green: 138/255, blue: 228/255, alpha: 1), gradientEndColor: UIColor(red: 246/255, green: 86/255, blue: 94/255, alpha: 1), shadowColor: UIColor(red: 198/255, green: 116/255, blue: 171/255, alpha: 1))
                return homeCollectionViewCell
                
            case 3:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "clientsIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 0/255, green: 210/255, blue: 58/255, alpha: 1), gradientEndColor: UIColor(red: 20/255, green: 203/255, blue: 206/255, alpha: 1), shadowColor: UIColor(red: 10/255, green: 206/255, blue: 136/255, alpha: 1))
                return homeCollectionViewCell
            default:
                let homeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                homeCollectionViewCell.initCell(image: UIImage(named: "chatsIcon")!, label: mainMenu?.data?.items?[indexPath.item].title ?? "", gradientStartColor: UIColor(red: 0/255, green: 210/255, blue: 58/255, alpha: 1), gradientEndColor: UIColor(red: 20/255, green: 203/255, blue: 206/255, alpha: 1), shadowColor: UIColor(red: 60/255, green: 182/255, blue: 217/255, alpha: 1))
                return homeCollectionViewCell
            }
        }
    }
}
