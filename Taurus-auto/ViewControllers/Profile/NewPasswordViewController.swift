//
//  NewPasswordViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 19.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class NewPasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var oldPasswordTextFieldView: UIView!
    @IBOutlet weak var newPasswordTextFieldView: UIView!
    
    @IBOutlet weak var wrongPasswordView: UIView!
    @IBOutlet weak var wrongPasswordMessage: UILabel!
    
    @IBOutlet weak var wrongOldPasswordView: UIView!
    @IBOutlet weak var wrongOldPasswordLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
        
    }
    
    func setupUi()  {
        self.nextButton.applyGradient(colors: [UIColor(red: 145/255, green: 111/255, blue: 239/255, alpha: 1).cgColor, UIColor(red: 94/255, green: 173/255, blue: 255/255, alpha: 1).cgColor])
        self.nextButton.layer.cornerRadius = 10
        self.nextButton.clipsToBounds = true
        oldPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        oldPasswordTextField.tag = 1
        newPasswordTextField.tag = 2
        navigationController?.navigationBar.isHidden = true
        self.oldPasswordTextField.addTarget(self, action: #selector(self.oldPasswordTextFieldDidChange), for: .editingChanged)
        self.newPasswordTextField.addTarget(self, action: #selector(self.newPasswordTextFieldDidChange), for: .editingChanged)
        addGesture()
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        view.endEditing(true)
        wrongPasswordView.isHidden = true
        wrongOldPasswordView.isHidden = true
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        let params = ["current_password" : oldPasswordTextField.text,
                      "new_password" : newPasswordTextField.text]
        DataManager.shared.userController.changePassword(params: params) { (response) in
            if response?.result == "ok" {
                self.navigationController?.popViewController(animated: true)
            } else {
                if response?.errors?[0].code == "constraints.current_password_check" {
                    self.wrongOldPasswordView.backgroundColor = UIColor(patternImage: UIImage(named: "popUpImage")!)
                    self.wrongOldPasswordView.isHidden = false
                    self.wrongOldPasswordLabel.text = response?.errors![0].message
                } else {
                    self.wrongPasswordView.backgroundColor = UIColor(patternImage: UIImage(named: "popUpImage")!)
                    self.wrongPasswordView.isHidden = false
                    self.wrongPasswordMessage.text = response?.errors![0].message
                }
            }
            
        }
    }
    
    @objc func oldPasswordTextFieldDidChange(_ textField: UITextField) {
        if oldPasswordTextField.text?.count ?? 0 >= 8 {
            self.oldPasswordTextField.setRightViewButton(icon:(UIImage(imageLiteralResourceName: "checkIcon")))
        }
        
    }
    
    @objc func newPasswordTextFieldDidChange(_ textField: UITextField) {
        if oldPasswordTextField.text == newPasswordTextField.text {
            self.newPasswordTextField.setRightViewButton(icon:(UIImage(imageLiteralResourceName: "checkIcon")))
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            
            oldPasswordTextFieldView.layer.borderWidth = 1
            oldPasswordTextFieldView.layer.borderColor = UIColor(red: 96/255, green: 147/255, blue: 253/255, alpha: 1).cgColor
            
        }else {
            newPasswordTextFieldView.layer.borderWidth = 1
            newPasswordTextFieldView.layer.borderColor = UIColor(red: 96/255, green: 147/255, blue: 253/255, alpha: 1).cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            oldPasswordTextFieldView.layer.borderWidth = 0
            oldPasswordTextFieldView.layer.borderColor = nil
        } else {
            newPasswordTextFieldView.layer.borderWidth = 0
            newPasswordTextFieldView.layer.borderColor = nil
        }
        
    }
}
