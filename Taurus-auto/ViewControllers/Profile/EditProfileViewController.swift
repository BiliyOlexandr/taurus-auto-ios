//
//  EditProfileViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 16.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var newNameField: UITextField!
    @IBOutlet weak var newPhoneField: UITextField!
    @IBOutlet weak var newEmailField: UITextField!
    @IBOutlet weak var linkPhoneButton: UIButton!
    @IBOutlet weak var phoneVerified: UIImageView!
    @IBOutlet weak var emailVerified: UIImageView!
    @IBOutlet weak var infoIcon: UIImageView!
    
    var currentUser:UserResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
        
    }
    
    func setupUi()  {
        navigationController?.navigationBar.isHidden = true
        currentUser = DataManager.shared.userController.currentUser
        newPhoneField.delegate = self
        self.newPhoneField.addTarget(self, action: #selector(self.newPhoneFieldTextFieldDidChange), for: .editingChanged)
        if currentUser?.data?.user?.status?.hasPhone ?? false {
            linkPhoneButton.isHidden = true
            phoneVerified.isHidden = false
            newPhoneField.text = currentUser?.data?.userData?.phoneNumber?.phoneNumber
        } else {
            linkPhoneButton.isHidden = false
            phoneVerified.isHidden = true
        }
        newNameField.text = (currentUser?.data?.user?.firstName ?? "") + " " + (currentUser?.data?.user?.lastName ?? "")
        
        if currentUser?.data?.userData?.email?.isConfirmed ?? false {
            emailVerified.isHidden = false
            infoIcon.isHidden = true
            newEmailField.text = currentUser?.data?.userData?.email?.email
        }
        
    }
    
    @objc func newPhoneFieldTextFieldDidChange(_ textField: UITextField) {
        if newPhoneField.text != "" {
            linkPhoneButton.isEnabled = true
        }
    }
    
    @IBAction func addNewNumberButtonPressed(_ sender: Any) {
        var params = ["phone" : newPhoneField.text?.trimmingCharacters(in: .whitespaces)]
        DataManager.shared.userController.changeUserPhone(params: params) { (response) in
            if response == nil {
               let alert = UIAlertController(title: "Error", message: "Internet connection error" , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
            if response?.result == "ok" {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "ConfirmNewPhoneViewController") as! ConfirmNewPhoneViewController
                vc.modalPresentationStyle = .fullScreen
                vc.providedPhone = self.newPhoneField.text
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func popViewController(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        var params = ["full_name" : newNameField.text,
                      "phone" : newPhoneField.text,
                      "email" : newEmailField.text]
        showSpinner(onView: view)
        DataManager.shared.userController.setUserProfileInfo(params: params) { (response) in
            if response == nil{
               let alert = UIAlertController(title: "Error", message: "Internet connection error" , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
            self.removeSpinner()
            if response?.result == "ok" {
                self.navigationController?.popViewController(animated: true)
            } else {
                if response?.errors?[0].code == "constraints.phone_confirmed_change_error"{
                    let parameters =  ["full_name" : self.newNameField.text,
                                       "email" : self.newEmailField.text]
                    DataManager.shared.userController.setUserProfileInfo(params: parameters) { (response) in
                        
                    }
                    //if self.currentUser?.data?.userData?.phoneNumber?.isConfirmed ?? true{
                    var params = ["phone" : self.newPhoneField.text?.trimmingCharacters(in: .whitespaces)]
                    DataManager.shared.userController.changeUserPhone(params: params) { (response) in
                        if response?.result == "ok" {
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "ConfirmNewPhoneViewController") as! ConfirmNewPhoneViewController
                            vc.modalPresentationStyle = .fullScreen
                            vc.providedPhone = self.newPhoneField.text
                            self.navigationController?.pushViewController(vc, animated: true)
                            //}
                        }
                    }
                }
            }
        }
        
    }
}
