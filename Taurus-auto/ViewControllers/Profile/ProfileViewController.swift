//
//  ProfileViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 16.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit
import QuartzCore

class ProfileViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var pickerBackGroundView: UIView!
    @IBOutlet weak var userProfilePhoto: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var switchNotificationButton: UIButton!
    @IBOutlet weak var timeZone: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    
    var currentUser: UserResponse?
    var avaliableTimezones: TimeZoneResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentUser = DataManager.shared.userController.currentUser
        avaliableTimezones = DataManager.shared.userController.avaliableTimeZones
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.getLoggedUserInfo { (user) in
            
            if user == nil{
                let alert = UIAlertController(title: "Error", message: "Internet connection error" , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
            self.currentUser = user
            self.setupUi()
        }
        if avaliableTimezones == nil {
            
            DataManager.shared.userController.getAvaliableTimeZones { (timezone) in
                self.removeSpinner()
                self.avaliableTimezones = timezone
                self.pickerView.reloadAllComponents()
            }
        } else {
            removeSpinner()
        }
        
        setupLabelTap()
        // Do any additional setup after loading the view.
    }
    
    func setupUi() {
        
        switchNotificationButton.isSelected = currentUser?.data?.userData?.noticeSettings?.pushNoticeEnabled ?? false
        switchNotificationButton.setImage(UIImage(named: "offSwitch"), for: .normal)
        switchNotificationButton.setImage(UIImage(named: "onSwitch"), for: .selected)
        
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backIcon")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        fullNameLabel.text = "\(currentUser?.data?.user?.firstName ?? "")"
        lastNameLabel.text = "\(currentUser?.data?.user?.lastName ?? "")"
        phoneLabel.text = String((currentUser?.data?.userData?.phoneNumber?.phoneNumber) ?? "")
        timeZone.text = currentUser?.data?.userData?.timezone?.title
        let timezonesArr = avaliableTimezones?.data?.timezones
        let index = timezonesArr?.firstIndex(where: { $0.title == currentUser?.data?.userData?.timezone?.title })
        let firstLetterFN = currentUser?.data?.user?.firstName?.substring(toIndex: 1)
        let firstLetterLN = currentUser?.data?.user?.lastName?.substring(toIndex: 1)
        userInitialsLabel.text = (firstLetterFN ?? "") + (firstLetterLN ?? "")
        userInitialsLabel.backgroundColor = UIColor(red: 45/255, green: 52/255, blue: 61/255, alpha: 1)
        userInitialsLabel.layer.masksToBounds = true
        userInitialsLabel.layer.cornerRadius = 20
        pickerView.selectRow(index ?? 0, inComponent: 0, animated: true)
        
        
    }
    
    @IBAction func notificationButton(_ sender: Any) {
        switchNotificationButton.isSelected = !switchNotificationButton.isSelected
        let params = ["push_notice_enabled" : switchNotificationButton.isSelected,
                      "email_notice_enabled" : switchNotificationButton.isSelected]
        DataManager.shared.userController.changeNotification(params: params) { (response) in
            if response?.result != "ok" {
                let alert = UIAlertController(title: "Error", message: response?.errors?[0].message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        DataManager.shared.userController.getLoggedUserInfo { (user) in
            self.currentUser = user
            self.setupUi()
        }
    }
    
    @IBAction func editProfile(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func dismissViewControllers() {
        
        guard let vc = self.presentingViewController else { return }
        
        while (vc.presentingViewController != nil) {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Вы действительно хотите выйти из аккаунта?", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Выйти", style: .destructive , handler:{ (UIAlertAction)in
            UserDefaults.standard.removeObject(forKey: "userToken")
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "loginNavigationController") as! UINavigationController
            vc.modalPresentationStyle = .fullScreen
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
            HHTabBarView.shared.referenceUITabBarController.dismiss(animated: true, completion: nil)
            
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            self.dismissViewControllers()
            UIApplication.shared.keyWindow?.rootViewController = vc
            self.view.window?.rootViewController = vc
            self.navigationController?.popToRootViewController(animated: true)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    @IBAction func changePassword(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "NewPasswordViewController") as! NewPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func labelTapped(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.pickerBackGroundView.alpha = 1.0
        }
    }
    
    @IBAction func showTimeZoneButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.pickerBackGroundView.alpha = 1.0
        }
    }
    
    @IBAction func pickerDoneButtonPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.pickerBackGroundView.alpha = 0.0
        }
        var params = ["zone_id" : avaliableTimezones?.data?.timezones?[pickerView.selectedRow(inComponent: 0)].zoneID]
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.setNewTimeZone(params: params) { (result) in
            self.removeSpinner()
            if result?.result == "ok" {
                self.timeZone.text = self.avaliableTimezones?.data?.timezones?[self.pickerView.selectedRow(inComponent: 0)].title
            } else {
                let alert = UIAlertController(title: "Error", message: result?.errors![0].message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func pickerCancelButtonPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.pickerBackGroundView.alpha = 0.0
        }
        
        
    }
    
    func setupLabelTap() {
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
        self.timeZone.isUserInteractionEnabled = true
        self.timeZone.addGestureRecognizer(labelTap)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return avaliableTimezones?.data?.timezones?.count ?? 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return avaliableTimezones?.data?.timezones?[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    
}

extension String {
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}
