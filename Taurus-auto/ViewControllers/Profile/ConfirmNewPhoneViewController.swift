//
//  ConfirmNewPhoneViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 17.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit
import PinCodeInputView


class ConfirmNewPhoneViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var pincodeView: UIView!
    @IBOutlet weak var helpTextLabel: UILabel!
    
    var providedPhone:String?
    let pinCodeInputView: PinCodeInputView<ItemView> = .init(
        digit: 6,
        itemSpacing: 4,
        itemFactory: {
            return ItemView()
    })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUi()
        self.addGesture()
        
    }
    func setupUi() {
        self.navigationController?.navigationBar.isTranslucent = true
        helpTextLabel.text = "Введите код подтверждения, который мы отправили вам на номер \(providedPhone ?? "")"
        pinCodeInputView.center = CGPoint(x: pincodeView.frame.width / 2, y: pincodeView.frame.height / 2)
        
        pincodeView.addSubview(pinCodeInputView)
        // set appearance
        pinCodeInputView.set(
            appearance: .init(
                itemSize: .init(width: 32, height: 44),
                font: .systemFont(ofSize: 24, weight: .regular),
                textColor: .black,
                backgroundColor: UIColor(red: 242/255, green: 245/255, blue: 247/255, alpha: 1),
                cursorColor: .clear,
                cornerRadius: 8
            )
        )
        pinCodeInputView.set(changeTextHandler: { text in
            if self.pinCodeInputView.isFilled {
                
                var params = ["phone" : self.providedPhone,
                              "code" : text]
                self.next(params: params as! [String : String])
            }
        })
        pinCodeInputView.becomeFirstResponder()
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView(_:)))
        view.addGestureRecognizer(tap)
        
        let tapPin = UITapGestureRecognizer(target: self, action: #selector(self.didTapPin(_:)))
        pincodeView.addGestureRecognizer(tapPin)
    }
    
    @objc func didTapView(_ gesture:UIGestureRecognizer) {
        pinCodeInputView.endEditing(true)
    }
    
    @objc func didTapPin(_ gesture:UIGestureRecognizer) {
        pinCodeInputView.becomeFirstResponder()
    }
    
    
    func next(params: [String : String]){
        
        self.showSpinner(onView: self.view)
        DataManager.shared.userController.changeUserPhone(params: params) { (response) in
            self.removeSpinner()
            if response?.result == "ok" {
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                let alert = UIAlertController(title: "Error", message: response?.errors?[0].message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @IBAction func repeateSendCode(_ sender: Any) {
        var params = ["phone" : providedPhone]
        DataManager.shared.userController.changeUserPhone(params: params) { (response) in
            
        }
    }
}

