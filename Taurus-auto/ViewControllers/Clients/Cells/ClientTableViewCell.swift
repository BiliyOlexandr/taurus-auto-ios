//
//  ClientTableViewCell.swift
//  Taurus-auto
//
//  Created by Taras Gural on 09.04.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class ClientTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var userInitialsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpUi()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpUi() {
        userInitialsLabel.backgroundColor = UIColor(red: 45/255, green: 52/255, blue: 61/255, alpha: 1)
        userInitialsLabel.text = "БП"
        userInitialsLabel.layer.masksToBounds = true
        userInitialsLabel.layer.cornerRadius = 10
    }
    
}
