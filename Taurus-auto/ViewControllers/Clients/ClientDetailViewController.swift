//
//  ClientDetailViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 09.04.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class ClientDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()

    }
    
    func setUpUi() {
        tableView.register(UINib(nibName: "ClientInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientInfoTableViewCell")
        tableView.register(UINib(nibName: "ClientRoleTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientRoleTableViewCell")
        tableView.register(UINib(nibName: "ClientActivationTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientActivationTableViewCell")
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0...3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClientInfoTableViewCell") as! ClientInfoTableViewCell
            cell.selectionStyle = .none
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClientRoleTableViewCell") as! ClientRoleTableViewCell
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClientActivationTableViewCell") as! ClientActivationTableViewCell
            cell.selectionStyle = .none
            return cell
        }
    }
}
