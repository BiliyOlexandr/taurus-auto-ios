//
//  ClientsViewController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 09.04.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class ClientsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
        // Do any additional setup after loading the view.
    }
    
    func setUpUi() {
        tableView.register(UINib(nibName: "SegmentControlTableViewCell", bundle: nil), forCellReuseIdentifier: "SegmentControlTableViewCell")
        tableView.register(UINib(nibName: "ClientTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientTableViewCell")
        tableView.separatorStyle = .none
        navigationController?.navigationBar.isHidden = true

    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Clients", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ClientDetailViewController") as! ClientDetailViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientTableViewCell") as! ClientTableViewCell
        cell.selectionStyle = .none
        return cell
    }
}
