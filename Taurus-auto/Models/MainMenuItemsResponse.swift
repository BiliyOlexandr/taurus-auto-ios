import Foundation

// MARK: - MainMenuItems
struct MainMenuItems: Codable {
    var result: String?
    var data: ItemData?
}

// MARK: - DataClass
struct ItemData: Codable {
    var items: [Item]?
}

// MARK: - Item
struct Item: Codable {
    var name, title: String?
}
