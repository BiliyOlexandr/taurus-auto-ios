// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let timeZoneResponse = try? newJSONDecoder().decode(TimeZoneResponse.self, from: jsonData)

import Foundation

// MARK: - TimeZoneResponse
struct TimeZoneResponse: Codable {
    let result, message: String?
    var errors: [Error]?
    let data: DataTimeZone?
}

// MARK: - DataClass
struct DataTimeZone: Codable {
    let timezones: [AvaliableTimezone]?
    let groups: [TimeZoneGroup]?
    let countries: [Country]?
}

// MARK: - Country
struct Country: Codable {
    let code, title: String?
}

// MARK: - Group
struct TimeZoneGroup: Codable {
    let name, title: String?
}

// MARK: - Timezone
struct AvaliableTimezone: Codable {
    let zoneID: Int?
    let title: String?

    enum CodingKeys: String, CodingKey {
        case zoneID = "zone_id"
        case title
    }
}
