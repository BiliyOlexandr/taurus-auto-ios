// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let userResponse = try? newJSONDecoder().decode(UserResponse.self, from: jsonData)

import Foundation

// MARK: - UserResponse
struct UserResponse: Codable {
    
    var result, message: String?
    var errors: [Error]?
    var data: DataClass?
}

// MARK: - Error
struct Error: Codable {
    var field, code, message: String?
}

// MARK: - DataClass
struct DataClass: Codable {
    var user: User?
    var userData: UserData?
    var accessToken: AccessToken?
    var access: AccessToken?

    enum CodingKeys: String, CodingKey {
        case user
        case userData = "user_data"
        case accessToken = "access_token"
        case access
    }
    
}

// MARK: - AccessToken
struct AccessToken: Codable {
    var accessToken: String?
    var token: String?
    var expire: Int?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expire
        case token
    }
}

// MARK: - User
struct User: Codable {
    var firstName: String?
    var groups: [Group]?
    var id, lastName: String?
    var status: Status?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case groups, id
        case lastName = "last_name"
        case status
    }
}

// MARK: - Group
struct Group: Codable {
    var name, title: String?
}

// MARK: - Status
struct Status: Codable {
    var hasEmail, hasPhone, hasCountry, active: Bool?

    enum CodingKeys: String, CodingKey {
        case hasEmail = "has_email"
        case hasPhone = "has_phone"
        case hasCountry = "has_country"
        case active
    }
}

// MARK: - UserData
struct UserData: Codable {
    var email: Email?
    var phoneNumber: PhoneNumber?
    var noticeSettings: NoticeSettings?
    var timezone: Timezone?

    enum CodingKeys: String, CodingKey {
        case email
        case phoneNumber = "phone_number"
        case noticeSettings = "notice_settings"
        case timezone
    }
}

// MARK: - Email
struct Email: Codable {
    var email: String?
    var isConfirmed: Bool?

    enum CodingKeys: String, CodingKey {
        case email
        case isConfirmed = "is_confirmed"
    }
}

// MARK: - NoticeSettings
struct NoticeSettings: Codable {
    var pushNoticeEnabled, emailNoticeEnabled: Bool?

    enum CodingKeys: String, CodingKey {
        case pushNoticeEnabled = "push_notice_enabled"
        case emailNoticeEnabled = "email_notice_enabled"
    }
}

// MARK: - PhoneNumber
struct PhoneNumber: Codable {
    var phoneNumber: String?
    var isConfirmed: Bool?

    enum CodingKeys: String, CodingKey {
        case phoneNumber = "phone_number"
        case isConfirmed = "is_confirmed"
    }
}

// MARK: - Timezone
struct Timezone: Codable {
    var zoneID: Int?
    var title: String?

    enum CodingKeys: String, CodingKey {
        case zoneID = "zone_id"
        case title
    }
}
