//
//  EndpointConstants.swift
//  Taurus-auto
//
//  Created by Taras Gural on 07.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import Foundation

struct Constants {
    static let serverDomain = "https://taurus.inbgroup.com/"
    static let authOrRegisterUserByPhone = "api/auth/register/phone"
}
