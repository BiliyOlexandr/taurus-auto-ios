//
//  UserController.swift
//  Taurus-auto
//
//  Created by Taras Gural on 05.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import Foundation
import Alamofire

class UserController {
    var currentUser:UserResponse?
    var avaliableTimeZones:TimeZoneResponse?
    var mainMenuItems:MainMenuItems?
    //TODO: Fix returnes
    func registerOrAuthoriseUser (params:[String : Any], completion: @escaping (UserResponse?) -> Void) {
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/auth/register/phone")!)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        //    request.setValue("X-Phone-Code", forHTTPHeaderField: <#T##String#>)
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                    let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                    let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                    self.currentUser = user
                    
                    UserDefaults.standard.set(user.data?.accessToken?.accessToken ?? user.data?.access?.token  , forKey: "userToken")
                    completion(user)
                    
                case .failure(let error):
                    completion(nil)
                }
        }
        
    }
    
    
    func requestAuthorisationCode (params:[String : Any], completion: @escaping (UserResponse? ) -> Void) {
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/auth/register/phone")!)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        let defaults = UserDefaults.standard
        let deviceToken:String = defaults.string(forKey: "deviceToken") ?? ""
        request.setValue(deviceToken, forHTTPHeaderField: "X-Phone-Code")
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    do{
                        let error = try JSONDecoder().decode(UserResponse.self, from: response.data!)
                        completion(error)
                        
                    }
                    catch let err {
                        completion(nil)
                    }
                case .failure(let error):
                    completion(nil)
                }
        }
        
    }
    
    func getResetPasswordCode (params:[String : Any], completion: @escaping (
        [String : Any]?) -> Void){
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/auth/reset/password")!)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        //               let deviceToken:String = defaults.string(forKey: "deviceToken") ?? ""
        //                 request.setValue(deviceToken, forHTTPHeaderField: "X-Phone-Code")
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(json)
                    } catch {
                        print(error)
                    }
                    
                case .failure(let error):
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(json)
                    } catch {
                        print(error)
                    }
                    
                }
        }
    }
    
    
    func changeUserPassword (params:[String : Any], completion: @escaping (UserResponse?) -> Void) {
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/auth/reset/password")!)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        //    request.setValue("X-Phone-Code", forHTTPHeaderField: <#T##String#>)
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                    let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                    let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                    self.currentUser = user
                    
                    UserDefaults.standard.set(user.data?.accessToken?.accessToken ?? user.data?.access?.token  , forKey: "userToken")
                    completion(user)
                    
                   
                    
                    
                case .failure(let error):
                    completion(nil)
                }
        }
        
    }
    
    func authoriseUser (params:[String : Any], completion: @escaping (UserResponse?) -> Void) {
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/auth/login")!)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        //    request.setValue("X-Phone-Code", forHTTPHeaderField: <#T##String#>)
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                        let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                        let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        self.currentUser = user
                        UserDefaults.standard.set(user.data?.accessToken?.accessToken ?? user.data?.access?.token  , forKey: "userToken")
                        completion(user)
                    
                case .failure(let error):
                    completion(nil)
                }
        }
        
    }
    
    func changeUserPhone(params:[String : Any], completion: @escaping (UserResponse?) -> Void){
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/users/me/phone")!)
        request.httpMethod = "POST"
        let token = UserDefaults.standard.string(forKey: "userToken")
        //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
        request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                  
                        let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                        let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(user)
                   
                    
                case .failure(let error):
                    print(error)
                    print(error.errorDescription)
                    completion(nil)
                }
        }
    }
    
    func getLoggedUserInfo(completion: @escaping (UserResponse?) -> Void){
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/users/me")!)
        request.httpMethod = "GET"
        let token = UserDefaults.standard.string(forKey: "userToken")
        //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
        request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        AF.request(request)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
               
                        let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                        let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(user)
                        self.currentUser = user
                    
                case .failure(let error):
                    print(error)
                    print(error.errorDescription)
                    completion(nil)
                }
        }
    }
    
    func changePassword(params:[String : Any], completion: @escaping (UserResponse?) -> Void){
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/users/me/password")!)
        request.httpMethod = "POST"
        let token = UserDefaults.standard.string(forKey: "userToken")
        //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
        request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                        let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                        let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(user)
                   
                    
                case .failure(let error):
                    print(error)
                    print(error.errorDescription)
                    completion(nil)
                }
        }
    }
    
    func getAvaliableTimeZones(completion: @escaping (TimeZoneResponse?) -> Void){
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/reference")!)
        request.httpMethod = "GET"
        let token = UserDefaults.standard.string(forKey: "userToken")
        //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
        request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        AF.request(request)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                        let user = try! JSONDecoder().decode(TimeZoneResponse.self, from: data)
                        let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(user)
                        self.avaliableTimeZones = user
                   
                case .failure(let error):
                    print(error)
                    print(error.errorDescription)
                    completion(nil)
                }
        }
    }
    
    func setNewTimeZone(params:[String : Any], completion: @escaping (TimeZoneResponse?) -> Void){
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/users/me/timezone")!)
        request.httpMethod = "POST"
        let token = UserDefaults.standard.string(forKey: "userToken")
        //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
        request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                        let user = try! JSONDecoder().decode(TimeZoneResponse.self, from: data)
                        let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(user)
                    
                    
                case .failure(let error):
                    print(error)
                    print(error.errorDescription)
                    completion(nil)
                }
        }
    }
    
    func setUserProfileInfo(params:[String : Any], completion: @escaping (UserResponse?) -> Void){
        var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/users/me")!)
        request.httpMethod = "POST"
        let token = UserDefaults.standard.string(forKey: "userToken")
        //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
        request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        request.httpBody = jsonData
        AF.request(request)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                        let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                        let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                        completion(user)
                   
                case .failure(let error):
                    print(error)
                    print(error.errorDescription)
                    completion(nil)
                }
        }
    }
    
    func changeNotification(params:[String : Any], completion: @escaping (UserResponse?) -> Void){
           var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/users/me/notice_settings")!)
           request.httpMethod = "POST"
           let token = UserDefaults.standard.string(forKey: "userToken")
           //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
           request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
           request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
           request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
           let jsonData = try? JSONSerialization.data(withJSONObject: params)
           request.httpBody = jsonData
        print(request.allHTTPHeaderFields)
           AF.request(request)
               .validate(statusCode: 200..<600)
               .responseJSON { response in
                   
                   switch response.result {
                   case .success:
                       guard let data = response.data else {
                           return
                       }
                       
                           let user = try! JSONDecoder().decode(UserResponse.self, from: data)
                           let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                           completion(user)
                      
                   case .failure(let error):
                       print(error)
                       print(error.errorDescription)
                       completion(nil)
                   }
           }
       }
    
    func getMainMenu(completion: @escaping (MainMenuItems?) -> Void){
           var request = URLRequest(url: URL(string: "https://taurus.inbgroup.com/api/menu/show")!)
           request.httpMethod = "GET"
           let token = UserDefaults.standard.string(forKey: "userToken")
           //request.setValue("X-Auth-Token", forHTTPHeaderField: token ?? "")
           request.setValue(token! , forHTTPHeaderField: "X-Auth-Token")
           request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
           request.setValue("user-agent", forHTTPHeaderField: "iPhone;Scale/3.00")
           AF.request(request)
               .validate(statusCode: 200..<600)
               .responseJSON { response in
                   
                   switch response.result {
                   case .success:
                       guard let data = response.data else {
                           return
                       }
                       
                           let user = try! JSONDecoder().decode(MainMenuItems.self, from: data)
                           let json = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                           completion(user)
                           self.mainMenuItems = user
                      
                   case .failure(let error):
                       print(error)
                       print(error.errorDescription)
                       completion(nil)
                   }
           }
       }
}
