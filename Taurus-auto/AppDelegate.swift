//
//  AppDelegate.swift
//  Taurus-auto
//
//  Created by Taras Gural on 05.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let hhTabBarView = HHTabBarView.shared
    let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    
    func setupReferenceUITabBarController() -> Void {
        
        let storyboard = UIStoryboard.init(name: "Home", bundle: Bundle.main)
        
        let navigationController1: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "HomeViewController"))
        let secondStoryboard = UIStoryboard.init(name: "Profile", bundle: Bundle.main)
        let thirdStoryboard = UIStoryboard.init(name: "Chat", bundle: Bundle.main)
        let navigationController2: UINavigationController = UINavigationController.init(rootViewController: secondStoryboard.instantiateViewController(withIdentifier: "ProfileViewController"))
        navigationController2.navigationBar.backIndicatorImage = UIImage(named: "backIcon")
        navigationController2.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        navigationController2.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        navigationController2.navigationBar.isHidden = true
        let navigationController3: UINavigationController = UINavigationController.init(rootViewController: thirdStoryboard.instantiateViewController(withIdentifier: "ChatRootViewController"))
        referenceUITabBarController.setViewControllers([navigationController1,navigationController3,navigationController2], animated: false)
        
        
    }
    
    func setupHHTabBarView() -> Void {
        //Create Custom Tabs
        let t1 = HHTabButton(withTitle: String(), tabImage: UIImage(named: "HH_home"), index: 0)
        t1.setImage(UIImage(named: "HH_home_active"), for: .selected)
        t1.imageVerticalAlignment = .center
        t1.imageHorizontalAlignment = .center
        
        let t2 = HHTabButton(withTitle: String(), tabImage: UIImage(named: "HH_chat"), index: 1)
        t2.setImage(UIImage(named: "HH_chat_selected"), for: .selected)
        t2.imageVerticalAlignment = .center
        t2.imageHorizontalAlignment = .center
        
        
        let t3 = HHTabButton(withTitle: String(), tabImage: UIImage(named: "HH_user"), index: 2)
        t3.setImage(UIImage(named: "HH_user_active"), for: .selected)
        t3.imageVerticalAlignment = .center
        t3.imageHorizontalAlignment = .center
        
        
        //Set Default Index for HHTabBarView.
        self.hhTabBarView.tabBarTabs = [t1,t2,t3]
        
        //You should modify badgeLabel after assigning tabs array.
        /*
         t1.badgeLabel?.backgroundColor = .white
         t1.badgeLabel?.textColor = selectedTabColor
         t1.badgeLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
         */
        
        //Show Animation on Switching Tabs
        hhTabBarView.tabChangeAnimationType = .pulsate
        
        //Set start tab
        hhTabBarView.defaultIndex = 0
        
        //Handle Tab Changes
        hhTabBarView.onTabTapped = { (tabIndex) in
            //            if tabIndex == 2 {
            //
            //            }
        }
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
        
        
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().backIndicatorImage = UIImage(named: "backIcon")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        UINavigationBar.appearance().tintColor = UIColor.black
        UIApplication.shared.registerForRemoteNotifications()
        
        
        do {
            sleep(2)
        }
        
        if #available(iOS 13.0, *) {
            
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            setupReferenceUITabBarController()
            setupHHTabBarView()
            
            let rootVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            if UserDefaults.standard.string(forKey: "userToken") != nil{
                DataManager.shared.userController.getLoggedUserInfo { (user) in
                    
                }
                
                DataManager.shared.userController.getAvaliableTimeZones { (timezone) in
                    
                }
                let rootNC = UINavigationController(rootViewController: referenceUITabBarController)
                self.window?.rootViewController = rootNC
            }else {
                self.window?.rootViewController = rootVC
            }
            
            self.window?.makeKeyAndVisible()
        }
        
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
    }
    
}

