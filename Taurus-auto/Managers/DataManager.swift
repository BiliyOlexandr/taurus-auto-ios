//
//  DataManager.swift
//  Taurus-auto
//
//  Created by Taras Gural on 05.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import Foundation

class DataManager {
    //MARK: Singleton
    static let shared = DataManager()
    
    var userController = UserController()
    var clientsController = ClientsController()
    
    
    private init() {
    }
}
