//
//  SceneDelegate.swift
//  Taurus-auto
//
//  Created by Taras Gural on 05.03.2020.
//  Copyright © 2020 InAppo. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    let hhTabBarView = HHTabBarView.shared
    let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    
    func setupReferenceUITabBarController() -> Void {
//        let homeViewController = HomeViewController()
//       
//        
//        self.referenceUITabBarController.setViewControllers([homeViewController], animated: false)
        
        let storyboard = UIStoryboard.init(name: "Home", bundle: Bundle.main)
        
        //Creating navigation controller for navigation inside the first tab.
        let navigationController1: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "HomeViewController"))
        let secondStoryboard = UIStoryboard.init(name: "Profile", bundle: Bundle.main)
         let thirdStoryboard = UIStoryboard.init(name: "Chat", bundle: Bundle.main)
         let navigationController2: UINavigationController = UINavigationController.init(rootViewController: secondStoryboard.instantiateViewController(withIdentifier: "ProfileViewController"))
         navigationController2.navigationBar.backIndicatorImage = UIImage(named: "backIcon")
        navigationController2.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backIcon")
        navigationController2.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
         let navigationController3: UINavigationController = UINavigationController.init(rootViewController: thirdStoryboard.instantiateViewController(withIdentifier: "ChatRootViewController"))
        navigationController2.navigationBar.isHidden = true
        
        referenceUITabBarController.setViewControllers([navigationController1,navigationController3,navigationController2], animated: false)


    }
    
     func setupHHTabBarView() -> Void {
            //Create Custom Tabs
            let t1 = HHTabButton(withTitle: String(), tabImage: UIImage(named: "HH_home"), index: 0)
            t1.setImage(UIImage(named: "HH_home_active"), for: .selected)
            t1.imageVerticalAlignment = .center
            t1.imageHorizontalAlignment = .center
            
            let t2 = HHTabButton(withTitle: String(), tabImage: UIImage(named: "HH_chat"), index: 1)
            t2.setImage(UIImage(named: "HH_chat_selected"), for: .selected)
            t2.imageVerticalAlignment = .center
            t2.imageHorizontalAlignment = .center
            
            
            let t3 = HHTabButton(withTitle: String(), tabImage: UIImage(named: "HH_user"), index: 2)
            t3.setImage(UIImage(named: "HH_user_active"), for: .selected)
            t3.imageVerticalAlignment = .center
            t3.imageHorizontalAlignment = .center
            
            
            //Set Default Index for HHTabBarView.
            self.hhTabBarView.tabBarTabs = [t1,t2,t3]
            
            //You should modify badgeLabel after assigning tabs array.
            /*
             t1.badgeLabel?.backgroundColor = .white
             t1.badgeLabel?.textColor = selectedTabColor
             t1.badgeLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
             */
            
            //Show Animation on Switching Tabs
            hhTabBarView.tabChangeAnimationType = .pulsate
            
            //Set start tab
            hhTabBarView.defaultIndex = 0
            
            //Handle Tab Changes
            hhTabBarView.onTabTapped = { (tabIndex) in
    //            if tabIndex == 2 {
    //
    //            }
            }
        }

    
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
//        if let data = UserDefaults.standard.value(forKey:"currentLoggedUser") as? Data {
//
//            self.window = UIWindow(windowScene: windowScene)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            setupReferenceUITabBarController()
            setupHHTabBarView()
        
            guard let rootVC = storyboard.instantiateViewController(identifier: "ViewController") as? ViewController else {
                print("ViewController not found")
                return
            }
        
        if UserDefaults.standard.string(forKey: "userToken") != nil{
            DataManager.shared.userController.getLoggedUserInfo { (user) in
                
            }
            
            DataManager.shared.userController.getAvaliableTimeZones { (timezone) in
                
            }
            let rootNC = UINavigationController(rootViewController: referenceUITabBarController)
            self.window?.rootViewController = rootNC
        }else {
            self.window?.rootViewController = rootVC
        }
            
            self.window?.makeKeyAndVisible()
        
    }
    
    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

